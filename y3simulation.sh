# R1
echo ""
echo "-----------------------------------------"
echo "R1 - STEP2"
echo -e "Simulating anastacia-mmt-untrusted \n  Event -> 2x 'Untrusted destination detected' \n  Alarm -> (11) 'MMT Probe - Potential data leakage detected' \n  Mitigation -> (1) 'Filter and deploy UTRC'"
echo "-----------------------------------------"
java -jar logger.jar 10.79.7.183 514 anastacia-mmt-untrusted 1000 2
echo "... 30secs ..."
sleep 30
# R2
echo ""
echo "-----------------------------------------"
echo "R2 - STEP4"
echo  -e "Simulating anastacia-utrc-mitm \n  Event -> 1x 'Data analysis report: Anomaly detected' \n  Alarm -> (8) 'Man in the Middle on IoT data' \n  Mitigation -> (2) 'Turnoff_Filter_And_Monitor'"
echo "-----------------------------------------"
java -jar logger.jar 10.79.7.183 514 anastacia-utrc-mitm
echo "... 120secs ..."
sleep 120
# R3
echo ""
echo "-----------------------------------------"
echo "R3 - STEP6"
echo  -e "Simulating anastacia-mmt-slowcomm \n  Event -> 2x 'MMT Probe - SlowComm attack detected' \n  Alarm -> (12) 'MMT Probe - Slow DDoS Attack Detected' \n  Mitigation -> (3) 'Isolation_Deploy_Honeynet_And_MMT_Monitor'"
echo "-----------------------------------------"
java -jar logger.jar 10.79.7.183 514 anastacia-mmt-slowcomm 1000 2
echo "... 180secs ..."
sleep 180
# R4
echo ""
echo "-----------------------------------------"
echo "R4 - STEP8"
echo -e "Simulating anastacia-mmt-untrusted \n  Event -> 2x 'Untrusted destination detected' \n  Alarm -> (11) 'MMT Probe - Potential data leakage detected' \n  Mitigation -> (4) 'Forward_Traffic_IoT_Honeynet'"
echo "-----------------------------------------"
java -jar logger.jar 10.79.7.183 514 anastacia-mmt-untrusted 1000 2
echo ""
echo "Done"
