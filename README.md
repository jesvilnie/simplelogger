### 1. **Files:**

- **logger.jar** is the Java Executable that sends log events to the XL-SIEM agent. Those logs are usually sent by the Probes and Sensors.

- **simpleLogger.conf**: Configuration file that contains the different event types.

- **random5secs.sh**: Bash script that sends a random event every 5 seconds.

- **y3simulation.sh**: Script that simulates the events for the Year 3 demo.



### 2. Configuration:

In the configuration file "simpleLogger.conf", the different event types are defined.

The configuration should look like this:

> events {
>
> event_code: "event_raw"
>
> }

   

### 3. Usage:

#### 	a. "logger.jar"

To list the different events types:

> java -jar logger.jar list

To send "n" events of type "event_type" every "interval" milliseconds to the "server_ip" in the port "port":

> java -jar logger.jar [server_ip] [port] [event_type] [interval] [n]

To send "n" random events every "interval" milliseconds to the "server_ip" in the port "port":

> java -jar logger.jar [server_ip] [port] random [interval] [n]

#### 	b. "random5secs.sh"

To execute the script and generate a random event every 5 seconds:

>  ./random5secs.sh



#### 	c. "y3simulation.sh"

To execute the script and generate the events that simulates the Y3 Demo attacks:

> ./y3simulation.sh

